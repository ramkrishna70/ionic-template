import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';
import { IonicModule } from '@ionic/angular';
import { Routes, RouterModule } from '@angular/router';
import { OnboardingPage } from './onboarding.page';

const routes: Routes = [
    { path: '', redirectTo: 'login', pathMatch: 'full' },
    { path: 'login', loadChildren: './login/login.module#LoginPageModule'},
    { path: 'signup', loadChildren: './signup/signup.module#SignupPageModule'}
]

@NgModule({
	imports: [
    	CommonModule,
    	FormsModule,
    	IonicModule, 
    	RouterModule.forChild(routes)
  	],
  	declarations: [OnboardingPage]
})
export class OnboardingPageModule {}
