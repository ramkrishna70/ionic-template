import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';
import { IonicModule } from '@ionic/angular';
import { Routes, RouterModule } from '@angular/router';
import { HomePage } from './home.page';
import { HeaderModule } from '@components/header/header.module';

const routes: Routes = [
	{ path: '', component: HomePage }
];
@NgModule({
	imports: [
    	CommonModule,
    	FormsModule,
    	IonicModule,
    	HeaderModule,
    	RouterModule.forChild(routes)
  	],
  	declarations: [HomePage]
})
export class HomePageModule {}
