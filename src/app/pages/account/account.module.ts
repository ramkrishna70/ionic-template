import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';
import { IonicModule } from '@ionic/angular';
import { Routes, RouterModule } from '@angular/router';
import { AccountPage } from './account.page';

const routes: Routes = [
    { path: '', redirectTo: 'myprofile', pathMatch: 'full' },
    { path: 'myprofile', loadChildren: './myprofile/myprofile.module#MyprofilePageModule'},
    { path: 'settings', loadChildren: './settings/settings.module#SettingsPageModule'}
  
]

@NgModule({
	imports: [
    	CommonModule,
    	FormsModule,
    	IonicModule,
        RouterModule.forChild(routes)
  	],
  	declarations: [AccountPage]
})
export class AccountPageModule {}
