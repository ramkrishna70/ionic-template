import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { SidemenuComponent } from './sidemenu.component';
import { IonicModule } from '@ionic/angular';

@NgModule({
  declarations: [SidemenuComponent],
  imports: [
    CommonModule,
    IonicModule
  ],
  exports: [SidemenuComponent]
})
export class SidemenuModule { }
