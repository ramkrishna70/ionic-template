import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { InlineinfoComponent } from './inlineinfo.component';
import { IonicModule } from '@ionic/angular';

@NgModule({
  declarations: [InlineinfoComponent],
  imports: [
    CommonModule,
    IonicModule
  ],
  exports: [InlineinfoComponent]
})
export class InlineinfoModule { }
